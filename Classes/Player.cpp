#include "Player.h"

USING_NS_CC;

bool Player::init() {
    _isForceEnable = true;
    _player = Sprite::create("1f60e.png");
    auto p1 = PhysicsBody::createCircle(_player->getContentSize().width / 2.f,  PhysicsMaterial(1.0f, 1.0f, 1.0f));

    _player->setPhysicsBody(p1);
    _player->setPosition(Size(960, 540) / 2.f);
    _player->getPhysicsBody()->setContactTestBitmask(0xFFFFFFFF);
    p1->setVelocityLimit(1000);
    p1->setAngularVelocityLimit(0);

    return true;
}

Sprite *Player::getPlayer() const {
    return _player;
}

void Player::move(Vec2 direction, USED_FORCE force) {       
    if (_isForceEnable) {
        switch (force) {
        case USED_FORCE::NONE:
//            _player->getPhysicsBody()->setVelocity(direction * 2);
            _player->getPhysicsBody()->setVelocity(Vec2(0, 0));
            _player->getPhysicsBody()->applyImpulse(5000 * direction);
            CCLOG("!%f", _player->getPhysicsBody()->getVelocity().x);
            CCLOG("!!!%f", _player->getPhysicsBody()->getVelocityLimit());
            break;
        case USED_FORCE::DIRECTED:
            _player->getPhysicsBody()->setVelocity(Vec2(0, 0));
            _player->getPhysicsBody()->applyImpulse(100000 * direction); //setVelocity(Vec2(direction * 100));
            this->runAction(Sequence::create(CallFunc::create([&]() { _isForceEnable = false;}), DelayTime::create(1.5), CallFunc::create([&]() { _isForceEnable = true;}), nullptr));
            break;
        default:

            break;
        }
    }

}
