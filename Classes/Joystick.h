/*
 * Joystick.h
 *
 *  Created on: 2015-2-1
 *      Author: yong
 */

#ifndef JOYSTICK_H_
#define JOYSTICK_H_

#include "cocos2d.h"
//using namespace cocos2d;

class JoystickEvent: public cocos2d::Ref {
private:
	JoystickEvent();

public:
	virtual ~JoystickEvent();
	virtual bool init();
	CREATE_FUNC(JoystickEvent);

public:
    static const std::string EVENT_JOYSTICK;
//    float mAnagle;
    float _data[3];
};

class Joystick: public cocos2d::Layer {
private:
    Joystick();

protected:
    bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);

public:
	virtual ~Joystick();
    virtual bool init();

    CREATE_FUNC(Joystick);
private:
    cocos2d::Sprite* mJsBg;
    cocos2d::Sprite* mJsCenter;
    cocos2d::Vec2 _mJsPos;
};

#endif /* JOYSTICK_H_ */
