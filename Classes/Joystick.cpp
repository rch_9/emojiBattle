/*
 * Joystick.cpp
 *
 *  Created on: 2015-2-1
 *      Author: yong
 */

#include "Joystick.h"

#define PI 3.1415926

USING_NS_CC;

const std::string JoystickEvent::EVENT_JOYSTICK = "event_of_joystick";

JoystickEvent::JoystickEvent():
    _data({0.f, 0.f, 0.f}) {
}

JoystickEvent::~JoystickEvent() {
}

bool JoystickEvent::init() {
	return true;
}

Joystick::Joystick():
    _mJsPos(Vec2(100, 100)) {
}

Joystick::~Joystick() {	
	_eventDispatcher->removeEventListenersForTarget(this);
}

bool Joystick::init() {
	bool result = false;

    do {
		if (!Layer::init()) {
			break;
        }

		mJsBg = Sprite::create("joystick_bg.png");
		if (nullptr == mJsBg) {
			break;
		}

        mJsBg->setPosition(_mJsPos);
		addChild(mJsBg);

		mJsCenter = Sprite::create("joystick_center.png");
		if (nullptr == mJsCenter) {
			break;
		}
        mJsCenter->setPosition(_mJsPos);
		addChild(mJsCenter);

		auto touchListener = EventListenerTouchOneByOne::create();
		if (nullptr == touchListener) {
			break;
		}

		touchListener->setSwallowTouches(true);
        touchListener->onTouchBegan = CC_CALLBACK_2(Joystick::onTouchBegan, this);
        touchListener->onTouchMoved = CC_CALLBACK_2(Joystick::onTouchMoved, this);
        touchListener->onTouchEnded = CC_CALLBACK_2(Joystick::onTouchEnded, this);
        _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

        result = true;
	} while (0);

	return result;
}

// Грибок ставится на место касания
bool Joystick::onTouchBegan(Touch *touch, Event *unused_event) {
    auto point = touch->getLocation();

    if (mJsCenter->getBoundingBox().containsPoint(point)) {
		return true;
    }

	return false;
}

void Joystick::onTouchMoved(Touch *touch, Event *unused_event) {
	Vec2 point = touch->getLocation();
    float y = point.y - _mJsPos.y;
    float x = point.x - _mJsPos.x;
    float angle = atan2(y, x) * 180 / PI;

    float jsBgRadis = mJsBg->getContentSize().width * 0.5;
    float distanceOfTouchPointToCenter = sqrt(pow(_mJsPos.x - point.x, 2) + pow(_mJsPos.y - point.y, 2));

    Vec2 buff;

	if (distanceOfTouchPointToCenter >= jsBgRadis) {        
        float deltX = x * (jsBgRadis / distanceOfTouchPointToCenter);
        float deltY = y * (jsBgRadis / distanceOfTouchPointToCenter);        
        buff.x = deltX;
        buff.y = deltY;
        mJsCenter->setPosition(Vec2(_mJsPos.x + deltX, _mJsPos.y + deltY));
	} else {        
        buff.x = point.x - _mJsPos.x;
        buff.y = point.y - _mJsPos.y;
		mJsCenter->setPosition(point);
	}

    JoystickEvent* jsEvent = JoystickEvent::create();
    jsEvent->_data[0] = angle;
    jsEvent->_data[1] = buff.x;
    jsEvent->_data[2] = buff.y;

    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(JoystickEvent::EVENT_JOYSTICK, jsEvent);
}

void Joystick::onTouchEnded(Touch *touch, Event *unused_event) {
    mJsCenter->setPosition(_mJsPos);

    JoystickEvent* jsEvent = JoystickEvent::create();
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(JoystickEvent::EVENT_JOYSTICK, jsEvent);
}
