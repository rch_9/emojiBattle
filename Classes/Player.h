#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "cocos2d.h"

enum class USED_FORCE {
    NONE = 0,
    DIRECTED,
    CIRCULAR
};

class Player : public cocos2d::Node {
public:
    virtual bool init() override;
    CREATE_FUNC(Player);
    cocos2d::Sprite *getPlayer() const;

    void move(cocos2d::Vec2 direction, USED_FORCE force = USED_FORCE::NONE);


private:
    bool _isForceEnable;
    cocos2d::Sprite* _player;
};

#endif // __PLAYER_H__
