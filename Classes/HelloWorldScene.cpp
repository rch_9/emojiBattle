#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "Joystick.h"
#include "Player.h"


#include "ui/CocosGUI.h"

USING_NS_CC;

Scene* HelloWorld::createScene() {
    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    scene->getPhysicsWorld()->setGravity(Vec2::ZERO);

    auto layer = HelloWorld::create();

    scene->addChild(layer);    

    return scene;
}

bool HelloWorld::init() {
    if (!Layer::init()) {
        return false;
    }       

    auto visibleSize = Director::getInstance()->getVisibleSize();

    auto hit1 = MenuItemImage::create("270b.png", "270b-1f3ff.png", CC_CALLBACK_1(HelloWorld::callbackHit1, this));
    hit1->setPosition(700, 80);

    auto hit2 = MenuItemImage::create("270a.png", "270a-1f3ff.png", CC_CALLBACK_1(HelloWorld::callbackHit2, this));
    hit2->setPosition(850, 80);

    auto menu = Menu::create(hit1, hit2, nullptr);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    auto node = Player::create();
    node->setTag(11);
    this->addChild(node); // ?
    this->addChild(node->getPlayer(), 1);

    auto s2 =  Sprite::create("1f60e.png");
    s2->setTag(12);
    auto p2 = PhysicsBody::createCircle(s2->getContentSize().width / 2.f,  PhysicsMaterial(1.0f, 1.0f, 1.0f));
    s2->setPhysicsBody(p2);
    s2->setPosition(visibleSize / 2.f + Size(300, 0));
    s2->getPhysicsBody()->setContactTestBitmask(0xFFFFFFFF);    
    this->addChild(s2, 1);

    auto ring = Sprite::create("267f.png");
    this->addChild(ring, 0);
    ring->setPosition(visibleSize / 2.f);

    auto wall = Node::create();
    wall->setPhysicsBody(PhysicsBody::createEdgeBox(visibleSize));
    wall->getPhysicsBody()->setContactTestBitmask(0xFFFFFFFF);
    wall->setPosition(visibleSize / 2.f);
    addChild(wall);

    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = CC_CALLBACK_1(HelloWorld::onContactBegin, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

    auto joystick = Joystick::create();
    this->addChild(joystick, 100);

    auto _listener = EventListenerCustom::create(JoystickEvent::EVENT_JOYSTICK, CC_CALLBACK_1(HelloWorld::jscallback, this));
    _eventDispatcher->addEventListenerWithFixedPriority(_listener, 1);

    this->scheduleUpdate();

    return true;
}

void HelloWorld::update(float dt) {

}

void HelloWorld::jscallback(EventCustom *pSender) {
    JoystickEvent* jsevent = static_cast<JoystickEvent*>(pSender->getUserData());
    auto player = static_cast<Player*>(this->getChildByTag(11));
    player->move(Vec2(jsevent->_data[1], jsevent->_data[2]));
//    CCLOG("%f", jsevent->_data[0]);
//    player->getPlayer()->setRotation(jsevent->_data[0]);
    _data[1] = jsevent->_data[1];
    _data[2] = jsevent->_data[2];
}

void HelloWorld::callback(Ref *pSender) {

}

void HelloWorld::callbackHit1(Ref *pSender) {
    CCLOG("hit1");
    auto player = static_cast<Player*>(this->getChildByTag(11));
    player->move(Vec2(_data[1], _data[2]), USED_FORCE::DIRECTED);    
//    player->getPhysicsBody()->setVelocity(Vec2::ZERO);
//    player->getPhysicsBody()->applyImpulse(Vec2(100000 * _data[1], 100000 * _data[2]));
}

void HelloWorld::callbackHit2(Ref *pSender) {
    CCLOG("hit2");
}

bool HelloWorld::onContactBegin(PhysicsContact& contact) {


    return true;
}
