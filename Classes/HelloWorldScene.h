#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init() override;
    virtual void update(float dt) override;

    void callback(cocos2d::Ref* pSender);
    void callbackHit1(cocos2d::Ref* pSender);
    void callbackHit2(cocos2d::Ref* pSender);
    void jscallback(cocos2d::EventCustom *pSender);
    bool onContactBegin(cocos2d::PhysicsContact &contact);

    CREATE_FUNC(HelloWorld);
private:
    float _data[3] = {0, 0, 0};
};

#endif // __HELLOWORLD_SCENE_H__
